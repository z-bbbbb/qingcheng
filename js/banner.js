window.onload = function () {
  const oBanner = document.querySelector(".banner");
  const oImgBox = document.querySelector(".imgBox");
  const aBtns = document.querySelectorAll(".pointBox li");
  let iNow = 1; 
  let timer = null;
  let isRuning = false; 

  for (let i = 0; i < aBtns.length; i++) {
    aBtns[i].index = i;
    aBtns[i].onclick = function () {
      iNow = this.index + 1;
      tab();
    };
  }
  oBanner.onmouseenter = function () {
    clearInterval(timer);
  };
  oBanner.onmouseleave = function () {
    timer = setInterval(function () {
      iNow++;
      tab();
    }, 2000);
  };


  timer = setInterval(function () {
    iNow++;
    tab();
  }, 2000);

  function tab() {
    if(!isRuning){
      isRuning = true;
    }
    for (let i = 0; i < aBtns.length; i++) {
      aBtns[i].className = "";
    }
    if (iNow == 6) {
      aBtns[0].className = "active";
    } else if(iNow == 0){
      aBtns[4].className = 'active';
    }else {
      aBtns[iNow - 1].className = "active";
    }

    startMove(oImgBox, { left: -2586 * iNow }, function () {
      if (iNow == 6) {
        iNow = 1;
        oImgBox.style.left = "-2586px";
      }else if(iNow == 0){
        iNow = 5;
        oImgBox.style.left = iNow * -2586 + 'px';
      }
    });

  }
};
