window.onload = function () {
    const oBanner = document.querySelector(".banner");
    const oUl = document.querySelector(".slideshow ,imgBox");
    const aBtns = document.querySelectorAll(".slideshow .pointBox li");
    let iNow = 1;
    let time = null;
    let isRunning = false;

    timerInner();

    for(var i = 0; i < aBtns.length; i ++) {
        aBtns[i].index = i;
        aBtns[i].onclick = function () {
            iNow = this.index + 1;
            tab();
        };
    }
    function timerInner() {
        timer = setInterval(function () {
            iNow++;
            tab();
        },2000);
    }
    function tab() {
         for(var i = 0; i < aBtns.length; i ++) {
             aBtns[i].className = "";
         }
         if(iNow == aBtns.length + 1) {
             aBtns[0].className = "active";

         }else if (iNow == 0) {
             aBtns[aBtns.length - 1].className = "active";

         }else {
             aBtns[iNow - 1].className = "active";
         }
         isRunning = true;
         startMove(oUl, { left: iNow * -2568}, function(){
             if (iNow == aBtns.length + 1) {
                 iNow = 1;
                 oUl.style.left = "-2568px";
             }else if (iNow == 0) {
                 iNow = 5;
                 oUl.style.left = iNow * -2568 + "px";
             }
             isRunning = false;
         });
    }
    oBanner.onmouseenter = function(){
        clearInterval(timer);
    };
    oBanner.onmouseleave = function () {
        timerInner();
    };
};